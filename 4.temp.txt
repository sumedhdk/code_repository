#include<stdio.h>

void read(float *f)
{
	printf("\nEnter the fahrenheit value:");
	scanf("%f",f);
}
void print(float *c)
{
	printf("\nThe celsius value is %.2f",c);
}

void commute(float *f,float *c)
{
	*c=(*f-32)*(5.0/9.0);
}

int main()
{
	float f,c;
	
	read(&f);
	commute(&f,&c);
	print(&c);
	
	return 0;
}
